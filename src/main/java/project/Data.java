package project;


public class Data {

	public static void main(String[] args) {

		MonitoredData d1 = new MonitoredData();
		// d1.readDataFromFile();
		d1.readData();

		d1.activityCountPerDay();
		d1.activityCountOverall();
		d1.countDistinctDays();
		d1.activityFilterDuration();
		d1.activityFilterLessThan5();

	}

}
