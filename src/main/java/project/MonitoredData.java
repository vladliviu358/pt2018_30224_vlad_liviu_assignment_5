package project;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class MonitoredData {

	private String startTime;
	private String endTime;
	private String activity;

	public List<MonitoredData> data = new ArrayList<MonitoredData>();

	public MonitoredData() {

	}

	public MonitoredData(String start, String end, String activity) {
		this.startTime = start;
		this.endTime = end;
		this.activity = activity;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String toString() {

		return this.startTime + " " + this.endTime + " " + this.activity;
	}

	public int getDay() {
		return Integer.parseInt(this.getStartTime().substring(8, 10));
	}

	public long getDuration() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

		LocalDateTime d1 = LocalDateTime.parse(this.getStartTime(), formatter);
		LocalDateTime d2 = LocalDateTime.parse(this.getEndTime(), formatter);
		return d1.until(d2, ChronoUnit.SECONDS);

	}

	@SuppressWarnings("resource")
	public void readDataFromFile() {

		MonitoredData item = null;
		String fileName = "Activities.txt";
		try {
			BufferedReader in = new BufferedReader(new FileReader(fileName));
			String line;
			while ((line = in.readLine()) != null) {
				String[] dataLine = line.split("\t");
				item = new MonitoredData();
				item.setStartTime(dataLine[0]);
				item.setEndTime(dataLine[2]);
				item.setActivity(dataLine[4]);
				data.add(item);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void readData() {

		String fileName = "Activities.txt";
		List<String> list = new ArrayList<>();

		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

			list = stream.collect(Collectors.toList());

			list.stream().forEach((c) -> {
				String[] s = c.split("\t\t");
				data.add(new MonitoredData(s[0], s[1], s[2]));
			});

		} catch (IOException e) {
			System.out.println("Nu se poate citi din fisier/fisierul nu exista");
			e.printStackTrace();
		}
	}

	public void countDistinctDays() {

		List<String> startTimeOnly = data.stream().map(s -> s.getStartTime()).collect(Collectors.toList());
		System.out.println(startTimeOnly);
		List<String> date = startTimeOnly.stream().map(s -> s.split(" ")).map(s -> new String(s[0]))
				.collect(Collectors.toList());
		System.out.println(date);
		List<String> days = date.stream().map(s -> s.split("-")).map(s -> new String(s[2]))
				.collect(Collectors.toList());
		System.out.println(days);
		System.out.println((int) days.stream().distinct().count());
	}

	public void activityCountOverall() {

		String filePath = "CountActivitiesOverall.txt";
		List<String> activityName = data.stream().map(s -> s.getActivity()).collect(Collectors.toList());
		System.out.println(activityName);
		Map<String, Long> activityMap = activityName.stream()
				.collect(Collectors.groupingBy(s -> s, Collectors.counting()));
		System.out.println(activityMap);
		try {
			Files.write(Paths.get(filePath), () -> activityMap.entrySet().stream()
					.<CharSequence>map(e -> e.getKey() + " - " + e.getValue()).iterator());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void activityCountPerDay() {

		String filePath = "CountActivitiesPerDay.txt";

		Map<Integer, Map<String, Long>> activityPerDay = data.stream().collect(Collectors.groupingBy(s -> s.getDay(),
				Collectors.groupingBy(s -> s.getActivity(), Collectors.counting())));
		System.out.println(activityPerDay);

		try {
			Files.write(Paths.get(filePath), () -> activityPerDay.entrySet().stream()
					.<CharSequence>map(e -> e.getKey() + " - " + e.getValue()).iterator());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void activityFilterDuration() {

		String filePath = "ActivityDurationLargerThan10.txt";
		int timeLimit = 36000;
		Map<String, Long> summing = data.stream()
				.collect(Collectors.groupingBy(s -> s.getActivity(), Collectors.summingLong(s -> s.getDuration())));
		System.out.println(summing);
		Map<String, Long> lessThan = summing.entrySet().stream().filter(s -> s.getValue() >= timeLimit)
				.collect(Collectors.toMap(s -> s.getKey(), s -> s.getValue() / 3600));
		System.out.println(lessThan);

		try {
			Files.write(Paths.get(filePath), () -> lessThan.entrySet().stream()
					.<CharSequence>map(e -> e.getKey() + " - " + e.getValue()).iterator());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void activityFilterLessThan5() {
		
		String filePath = "ActivityFilterLessThan5.txt";
		Map<String, List<Long>> duration = data.stream().collect(Collectors.groupingBy(s -> s.getActivity(),
				Collectors.mapping(s -> s.getDuration(), Collectors.toList())));
		System.out.println(duration);
		List<String> activitiesLessThan5 = new ArrayList<>();
		duration.forEach((key, value) -> {
			int contorValori = 0;
			for (Long val : value)
				if (val < 300)
					contorValori++;
			if (contorValori / value.size() >= 0.9)
				activitiesLessThan5.add(key);
		});
		System.out.println(activitiesLessThan5);
		try {
			Files.write(Paths.get(filePath), activitiesLessThan5);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
